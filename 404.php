<?php
/**
 * Template Name: 404
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$templates = ['404.twig'];

Timber::render( $templates, $context );