<?php
// This functions file is for all custom blocks added via ACF
// Reference: https://www.advancedcustomfields.com/resources/acf_register_block_type/

if( function_exists('acf_register_block_type') ) :
	include 'acf-blocks-callback.php'; // pass-off to let Timber render the blocks

	// accessible and dynamic dropdown block
	$dropdown_block = [
		'name' => 'dropdown-block',
		'title' => __( 'Dropdown Block', 'mod' ),
		'description' => __( 'Creates a dropdown container; The content is folded into the dropdown title.', 'mod' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'mod-blocks',
		'align' => 'center',
		'icon' => 'sort',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'drop', 'down', 'dropdown', 'accordion', 'block' ]
	];
	acf_register_block_type( $dropdown_block );

	$blue_pill_button = [
		'name' => 'blue-pill-button',
		'title' => __( 'Blue Pill Button', 'mod' ),
		'description' => __( 'Creates a blue, pill-style button.', 'mod' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'mod-blocks',
		'align' => 'center',
		'icon' => 'button',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'blue', 'pill', 'button', 'link' ]
	];
	acf_register_block_type( $blue_pill_button );

	$red_pill_button = [
		'name' => 'red-pill-button',
		'title' => __( 'Red Pill Button', 'mod' ),
		'description' => __( 'Creates a red, pill-style button.', 'mod' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'mod-blocks',
		'align' => 'center',
		'icon' => 'button',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'red', 'pill', 'button', 'link' ]
	];
	acf_register_block_type( $red_pill_button );

	$yellow_pill_button = [
		'name' => 'yellow-pill-button',
		'title' => __( 'Yellow Pill Button', 'mod' ),
		'description' => __( 'Creates a yellow pill-style button.', 'mod' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'mod-blocks',
		'align' => 'center',
		'icon' => 'button',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'yellow', 'pill', 'button', 'link' ]
	];
	acf_register_block_type( $yellow_pill_button );

	$orange_pill_button = [
		'name' => 'orange-pill-button',
		'title' => __( 'Orange Pill Button', 'mod' ),
		'description' => __( 'Creates an orange, pill-style button.', 'mod' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'mod-blocks',
		'align' => 'center',
		'icon' => 'button',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'white', 'orange', 'pill', 'button', 'link' ]
	];
	acf_register_block_type( $orange_pill_button );

	$teal_pill_button = [
		'name' => 'teal-pill-button',
		'title' => __( 'Teal Pill Button', 'mod' ),
		'description' => __( 'Creates a teal, pill-style button.', 'mod' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'mod-blocks',
		'align' => 'center',
		'icon' => 'button',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'white', 'teal', 'pill', 'button', 'link' ]
	];
	acf_register_block_type( $teal_pill_button );

	$pill_button = [
		'name' => 'pill-button',
		'title' => __( 'Pill Button', 'mod' ),
		'description' => __( 'Creates a blue, pill-style button with modifiable foreground and background colors.', 'mod' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'mod-blocks',
		'align' => 'left',
		'icon' => 'button',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'pill', 'button', 'link' ]
	];
	acf_register_block_type( $pill_button );

	$fifty_fifty_block = [
		'name' => 'fifty-fifty-block',
		'title' => __( '50/50 Full Width', 'mod' ),
		'description' => __( 'Creates a full width block that is 50% yellow (left) and 50% teal (right).', 'mod' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'mod-blocks',
		'align' => 'full',
		'icon' => 'image-flip-horizontal',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ '50', 'fifty', 'yellow', 'teal', 'full', 'width', 'block' ]
	];
	acf_register_block_type( $fifty_fifty_block );

	$testimonial_block = [
		'name' => 'testimonial-block',
		'title' => __( 'Full Width Testimonial', 'mod' ),
		'description' => __( 'Creates a full width block that allows you to enter a testimonial or review.', 'mod' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'mod-blocks',
		'align' => 'full',
		'icon' => 'format-quote',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'testimonial', 'review', 'full', 'width', 'block' ]
	];
	acf_register_block_type( $testimonial_block );

	$statistic_grid = [
		'name' => 'statistic-grid',
		'title' => __( 'Statistic Grid', 'mod' ),
		'description' => __( 'Creates a full width block that allows you to enter up to 5 statistics in a grid.', 'mod' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'mod-blocks',
		'align' => 'wide',
		'icon' => 'grid-view',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'statistic', 'grid', 'block', 'point', 'stat' ]
	];
	acf_register_block_type( $statistic_grid );
endif;