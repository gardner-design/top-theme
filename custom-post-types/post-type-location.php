<?php
$labels = [
	'name'                => __( 'Locations', 'mod' ),
	'singular_name'       => __( 'Location', 'mod' ),
	'add_new'             => _x( 'Add Location', 'mod', 'mod' ),
	'add_new_item'        => __( 'Add Location', 'mod' ),
	'edit_item'           => __( 'Edit Location', 'mod' ),
	'new_item'            => __( 'Add Location', 'mod' ),
	'view_item'           => __( 'View Location', 'mod' ),
	'search_items'        => __( 'Search Locations', 'mod' ),
	'not_found'           => __( 'No Locations found', 'mod' ),
	'not_found_in_trash'  => __( 'No Locations found in Trash', 'mod' ),
	'parent_item_colon'   => __( 'Parent Location:', 'mod' ),
	'menu_name'           => __( 'Locations', 'mod' ),
];

$args = [
	'labels'              => $labels,
	'hierarchical'        => false,
	'description'         => 'Locations Post Type.',
	'taxonomies'          => [],
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'show_in_rest'		  => true,
	'menu_position'       => null,
	'menu_icon'           => 'dashicons-admin-multisite',
	'show_in_nav_menus'   => true,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'has_archive'         => true,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => [ 'title', 'thumbnail' ]
];

register_post_type( 'location', $args );