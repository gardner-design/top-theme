<?php
$labels = [
	'name'                => __( 'Testimonials', 'mod' ),
	'singular_name'       => __( 'Testimonial', 'mod' ),
	'add_new'             => _x( 'Add Testimonial', 'mod', 'mod' ),
	'add_new_item'        => __( 'Add Testimonial', 'mod' ),
	'edit_item'           => __( 'Edit Testimonial', 'mod' ),
	'new_item'            => __( 'Add Testimonial', 'mod' ),
	'view_item'           => __( 'View Testimonial', 'mod' ),
	'search_items'        => __( 'Search Testimonials', 'mod' ),
	'not_found'           => __( 'No Testimonials found', 'mod' ),
	'not_found_in_trash'  => __( 'No Testimonials found in Trash', 'mod' ),
	'parent_item_colon'   => __( 'Parent Testimonial:', 'mod' ),
	'menu_name'           => __( 'Testimonials', 'mod' ),
];

$args = [
	'labels'              => $labels,
	'hierarchical'        => false,
	'description'         => 'Customer Testimonials Post Type.',
	'taxonomies'          => [],
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'show_in_rest'		  => true,
	'menu_position'       => null,
	'menu_icon'           => 'dashicons-star-half',
	'show_in_nav_menus'   => true,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'has_archive'         => true,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => [ 'title', 'thumbnail' ]
];

register_post_type( 'testimonial', $args );