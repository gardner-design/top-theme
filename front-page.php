<?php
/**
 * Template Name: Home Template
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

// get 1 random testimonial to display
$context['testimonials'] = Timber::get_posts([
	'post_type' => 'testimonial',
	'posts_per_page' => 1,
	'orderby' => 'rand'
]);

$templates = ['front-page.twig'];

Timber::render( $templates, $context );