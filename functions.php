<?php

// change 'views' directory to 'templates'
Timber::$locations = __DIR__ . '/templates';

class MODSite extends TimberSite {

	function __construct() {
		// Action Hooks //
		add_action( 'after_setup_theme', [ $this, 'after_setup_theme' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'enqueue_scripts' ] );
		add_action( 'admin_head', [ $this, 'admin_head_css' ] );
		add_action( 'admin_menu', [ $this, 'admin_menu_cleanup'] );
		add_action( 'admin_menu', [ $this, 'global_site_options_menu' ] );
		add_action( 'init', [ $this, 'register_post_types' ] );
		add_action( 'login_enqueue_scripts', [ $this, 'style_login' ] );
		add_action( 'acf/init', [ $this, 'render_custom_acf_blocks' ] );

		// Filter Hooks //
		add_filter( 'timber_context', [ $this, 'add_to_context' ] );
		add_filter( 'block_categories', [ $this, 'mod_block_category' ], 10, 2 );
		add_filter( 'manage_edit-page_columns', [ $this, 'disable_admin_columns' ] );

		parent::__construct();
	}

	// hide WP update nag and plugin banners
	function admin_head_css() {
		?>
		<style type="text/css">
			.update-nag { display: none !important; }
			#wp-admin-bar-comments { display: none !important; }
			#toplevel_page_acf-options-theme-options .wp-submenu .wp-first-item { display: none !important; }
		</style>
		<?php
	}

	// WP admin login styles
	function style_login() {
		?>
		<style type="text/css">
			#login h1, .login h1 {
				background-color: transparent;
				padding: 0.75rem 0.5rem;
				border-radius: 2px;
			}

			#login h1 a, .login h1 a {
				background-image: url('<?= get_stylesheet_directory_uri() . '/static/images/top-logo.png' ?>') !important;
				background-position: center;
				width: 10rem;
				background-size: contain;
				padding: 1rem;
				margin: 0 auto;
			}
		</style>
		<?php
	}

	// enqueue styles & scripts
	function enqueue_scripts() {
		$version = filemtime( get_stylesheet_directory() . '/style-dist.css' );
		wp_enqueue_style( 'mod-css', get_stylesheet_directory_uri() . '/style-dist.css', [], $version );
		wp_enqueue_style( 'block-css', get_stylesheet_directory_uri() . '/block-style-dist.css', [], $version );
		wp_enqueue_script( 'mod-js', get_template_directory_uri() . '/static/js/site-dist.js', ['jquery'], $version);
	}

	// Custom context helper functions (callable)
	function add_to_context( $context ) {
		$context['site']           = $this;
		$context['date']           = date( 'F j, Y' );
		$context['date_year']      = date( 'Y' );
		$context['options']        = get_fields( 'option' );
		$context['home_url']       = home_url( '/' );
		$context['is_front_page']  = is_front_page();
		$context['get_url']        = $_SERVER['REQUEST_URI'];

		return $context;
	}

	// Menus / Theme Support / ACF Options Page
	function after_setup_theme() {
		register_nav_menu( 'primary', 'Main Nav' );
		register_nav_menu( 'footer', 'Footer Nav' );

		add_theme_support( 'menus' );
		add_theme_support( 'align-wide' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'editor-styles' );
		add_theme_support( 'disable-custom-colors' ); // disable color picker wheel

		add_editor_style( 'block-style-dist.css' );

		// setup colors array
		$colors = [
			[
				'name'  => __( 'Grey', 'mod' ),
				'slug'  => 'grey',
				'color' => '#757575'
			],

			[
				'name'  => __( 'Red', 'mod' ),
				'slug'  => 'red',
				'color' => '#DE3F49'
			],

			[
				'name'  => __( 'Green', 'mod' ),
				'slug'  => 'green',
				'color' => '#8AAD46'
			],

			[
				'name'  => __( 'Blue', 'mod' ),
				'slug'  => 'blue',
				'color' => '#5C86C0'
			],

			[
				'name'  => __( 'Yellow', 'mod' ),
				'slug'  => 'yellow',
				'color' => '#F9BF3E'
			],

			[
				'name'  => __( 'Orange', 'mod' ),
				'slug'  => 'orange',
				'color' => '#F89E29'
			],

			[
				'name'  => __( 'Teal', 'mod' ),
				'slug'  => 'teal',
				'color' => '#87B7B4'
			],

			[
				'name'  => __( 'Cream', 'mod' ),
				'slug'  => 'cream',
				'color' => '#F6F1E7'
			],

			[
				'name'  => __( 'Dark Grey', 'mod' ),
				'slug'  => 'dark-grey',
				'color' => '#424242'
			]
		];
		add_theme_support( 'editor-color-palette', $colors ); // add custom color palette (theme colors - leave empty to disable all color options)

		// create Global Options page for things like footer data and company info/logos
		if( function_exists( 'acf_add_options_page' ) ) {
			$parent = acf_add_options_page([
				'page_title'      => 'Theme Options',
				'menu_title'      => 'Theme Options',
				'capability'      => 'edit_posts',
				'redirect'        => false,
				'updated_message' => 'Global Options Updated.',
			]);

			// General Settings
			$child = acf_add_options_sub_page([
				'page_title'  => __( 'General Settings' ),
				'menu_title'  => __( 'General' ),
				'parent_slug' => $parent['menu_slug'],
			]);

			// Social Settings
			$child = acf_add_options_sub_page([
				'page_title'  => __( 'Social Settings' ),
				'menu_title'  => __( 'Social' ),
				'parent_slug' => $parent['menu_slug'],
			]);

			// Contact Settings
			$child = acf_add_options_sub_page([
				'page_title'  => __( 'Contact Settings' ),
				'menu_title'  => __( 'Contact' ),
				'parent_slug' => $parent['menu_slug'],
			]);

			// 404 Settings
			$child = acf_add_options_sub_page([
				'page_title'  => __( '404 Settings' ),
				'menu_title'  => __( '404' ),
				'parent_slug' => $parent['menu_slug'],
			]);
		}
	}

	// registers and renders our custom acf blocks (this function has a failsafe if ACF is not installed)
	function render_custom_acf_blocks() {
		require 'acf-block-functions.php';
	}

	// creates a custom block category for our theme-specific blocks
	function mod_block_category( $categories, $post ) {
		return array_merge(
			$categories, [
				[
					'slug'  => 'mod-blocks',
					'title' => 'TOP Custom Blocks',
				],
			]
		);
	}

	function register_post_types() {
		include_once('custom-post-types/post-type-testimonial.php');
		include_once('custom-post-types/post-type-location.php');
	}

	// remove unused items from top toolbar
	function disable_admin_columns( $columns ) {
		unset( $columns['comments'] );
		return $columns;
	}

	// remove unused items from admin menu
	function admin_menu_cleanup() {
		remove_menu_page( 'edit.php' ); // Posts
		remove_menu_page( 'edit-comments.php' ); // Comments
	}
} // End of MODSite class

new MODSite();

// main site nav (used to render the menu in /templates/navigation.twig)
function mod_render_primary_menu() {
	wp_nav_menu([
		'theme_location' => 'primary',
		'container'      => false,
		'menu_id'        => 'primary-menu'
	]);
}

function mod_render_footer_menu() {
	wp_nav_menu([
		'theme_location' => 'footer',
		'container'		 => false,
		'menu_id'		 => 'footer-menu'
	]);
}

// move our ACF Options Page below the Dashboard tab
function custom_menu_order( $menu_ord ) {
	if( ! $menu_ord ) {
		return true; // bail early
	}

	// define our menu item
	$menu = 'acf-options-theme-options';

	// remove from current menu
	$menu_ord = array_diff( $menu_ord, [$menu] );

	// append after index i[0]
	array_splice( $menu_ord, 1, 0, [$menu] );

	return $menu_ord;
}
add_filter( 'custom_menu_order', 'custom_menu_order' );
add_filter( 'menu_order', 'custom_menu_order' );

// disable the block editor on certain templates
function mod_disable_editor( $id = false ) {
	$excluded_templates = [
		'front-page.php'
	];

	if( empty( $id ) )
		return false;

	$id = intval( $id );
	$template = get_page_template_slug( $id );

	return in_array( $template, $excluded_templates );
}

// send back the correct "can-edit" property
function mod_disable_gutenberg( $can_edit, $post_type ) {
	if( ! ( is_admin() && !empty( $_GET['post'] ) ) )
		return $can_edit;

	if( mod_disable_editor( $_GET['post'] ) )
		$can_edit = false;

	return $can_edit;
}
add_filter( 'gutenberg_can_edit_post_type', 'mod_disable_gutenberg', 10, 2 );
add_filter( 'use_block_editor_for_post_type', 'mod_disable_gutenberg', 10, 2 );