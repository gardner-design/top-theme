<?php
/**
 * Default Template.
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

// get locations to display
$context['locations'] = Timber::get_posts([
	'post_type' => 'location',
	'posts_per_page' => -1,
	'orderby' => 'title',
	'order' => 'ASC'
]);

$templates = ['page.twig'];

Timber::render( $templates, $context );