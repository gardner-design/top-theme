(function($) {
    $(document).ready(function() {

		let $body = $('body'); // helper var

		$body.scroll( function() {
			// basic fadein function
			$('.fadeout').each( function(i) {

				var top_of_object = $(this).offset().top;
				var bottom_of_window = $(window).scrollTop() + $(window).height() * (3/4);

				if( bottom_of_window > top_of_object ) {
					$(this).removeClass('fadeout');
					$(this).addClass('fadein');
				}
			});
		});

		// close function for notice box (base.twig - homepage only)
		$('.close-notice').click(function() {
			$(this).parent().hide();
			$('.nav-wrapper').removeClass('adjust-wrap');
			$('.logo-container').removeClass('adjust-top');
			sessionStorage.setItem('closed_banner', 'true');
		});

		$(function siteNavigation() {
			var notice_banner = $('.notice-banner');
			var is_open = !!notice_banner; // cast to bool value (true if found in DOM)

			if( $body.width() < 960 ) {
				if( is_open && !sessionStorage.getItem('closed_banner') ) {
					$('.nav-wrapper').addClass('adjust-wrap');
					$('.logo-container').addClass('adjust-top');
				} else {
					notice_banner.hide();
					$('.nav-wrapper').removeClass('adjust-wrap');
					$('.logo-container').removeClass('adjust-top');
				}
			} else if( sessionStorage.getItem('closed_banner') ) {
				notice_banner.hide();
			}

			// mobile nav functions/handlers
			$('#menu-toggle').click(function() {
				$('.x-bar').toggleClass('x-bar-active');
				$('.mobile-nav-wrapper').toggleClass('mobile-nav-open');

				if( is_open ) {
					$('.notice-banner').hide();
					$('.nav-wrapper').removeClass('adjust-wrap');
					$('.logo-container').removeClass('adjust-top');
				}

				// body scroll lock when nav is open
				if( $('.mobile-nav-wrapper').hasClass('mobile-nav-open') ) {
					$body.css('overflow-y', 'hidden');
					$body.css('height', '100%');
				} else {
					$body.css('overflow-y', 'auto');
				}
			});

			// make sure we are on mobile since we are targeting classes
			if( $body.width() < 960 ) {
				// grab the height of each sub-menu for the slide function, then collapse it
				$('.sub-menu:first').each(function() {
					$height = $(this).height();
					$(this).css('height', $height);
					$(this).hide();
				});

				$('.menu-item-has-children').click(function() {
					var $sub = $(this).find('.sub-menu:first');
					$sub.slideToggle(350);
					$(this).children('a').toggleClass('chevron-rotate');
				});
			}
		});

		// Footer locations collapse on mobile (under 700px)
		if( $body.width() < 700 ) {
			$('.location #loc-info').each(function() {
				$l_height = $(this).height();
				$(this).css('height', $l_height);
				$(this).hide();
			});

			$('.location .h4').click(function() {
				$(this).toggleClass('chevron-rotate');
				$(this).next('#loc-info').slideToggle(350);
			});
		}

		// accessible dropdown block - controls and aria events for screenreaders
		$(function dropDownBlock() {
			$('.dropdown-content').each(function() {
				$(this).hide();
			});

			$('.dropdown-block-title').click(function() {
				var $this = $(this);
				// fires on first click (content is expanded)
				if( $this.hasClass('target') ) {
					$this.toggleClass('remove-border');
					$this.removeClass('target');
					$this.attr('aria-pressed', 'true');
					$this.next('.dropdown-content').slideToggle(350);
					$this.next('.dropdown-content').attr('aria-expanded', 'true');
				} else {
					// fires on second click (content is closed)
					$this.next('.dropdown-content:first').slideToggle(350, function() {
						$this.prev('.dropdown-block-title').addClass('target');
						$this.prev('.dropdown-block-title').attr('aria-pressed', 'false');
						$this.attr('aria-expanded', 'false');
						$this.toggleClass('remove-border');
					});
				}
				// always fire
				$(this).toggleClass('chevron-rotate');
			});
		});

	}); // end Document.Ready
})(jQuery);

// non jQuery JS goes here.
// If you need to load/use another library - make a new js document to reduce the loading time from 2 libraries in 1 file.
// This also prevents render-blocking